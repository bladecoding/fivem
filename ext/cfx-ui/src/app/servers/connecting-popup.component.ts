import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { Translation, TranslationService } from 'angular-l10n';

@Component({
	moduleId:    module.id,
	selector:    'connecting-popup',
	templateUrl: 'connecting-popup.component.html',
	styleUrls:   ['connecting-popup.component.scss']
})
export class ConnectingPopupComponent extends Translation implements OnInit {
	showOverlay = false;
	overlayClosable = true;
	overlayTitle: string;
	overlayMessage: string;
	overlayMessageData = {};
	closeLabel = "#Servers_CloseOverlay";
	helpLabel = "#Servers_HelpOverlay";
	helpLink: string = null;
	

	constructor(
		private gameService: GameService,
		public translation: TranslationService
	) {
		super();
	}

	ngOnInit() {
		this.gameService.connecting.subscribe(a => {
			this.overlayTitle = '#Servers_Connecting';
			this.overlayMessage = '#Servers_ConnectingTo';
			this.overlayMessageData = {serverName: a.address};
			this.showOverlay = true;
			this.overlayClosable = false;
			this.helpLink = null;
		});

		this.gameService.connectFailed.subscribe(([server, message]) => {
			this.overlayTitle = '#Servers_ConnectFailed';
			this.overlayMessage = '#Servers_Message';
			this.overlayMessageData = {message};
			this.showOverlay = true;
			this.overlayClosable = true;
			this.closeLabel = "#Servers_CloseOverlay";
			this.helpLink = this.getLinkFromMessage(message);
		});

		this.gameService.connectStatus.subscribe(a => {
			this.overlayTitle = '#Servers_Connecting';
			this.overlayMessage = '#Servers_Message';
			this.overlayMessageData = {message: a.message};
			this.showOverlay = true;
			this.helpLink = null;
			this.overlayClosable = (a.count == 133 && a.total == 133); // magic numbers, yeah :(

			if (this.overlayClosable) {
				this.closeLabel = "#Servers_CancelOverlay";
			}
		});

		this.gameService.errorMessage.subscribe(message => {
			this.overlayTitle = '#Servers_Error';
			this.overlayMessage = '#Servers_Message';
			this.overlayMessageData = {message};
			this.showOverlay = true;
			this.overlayClosable = true;
			this.closeLabel = "#Servers_CloseOverlay";
			this.helpLink = this.getLinkFromMessage(message);
		});
	}

	closeOverlay() {
		this.showOverlay = false;

		this.gameService.cancelNativeConnect();
	}
	
	getLinkFromMessage(msg) {
		//Must be https and have a tld. Only show the button when there is exactly 1 url.
		var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var matches = (msg || "").match(expression);
		if (matches == null || matches.length !== 1)
			return null;
		return matches[0];
	}
	
	showHelp() {
		this.gameService.openUrl(this.helpLink);
	}
}
