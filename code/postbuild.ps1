$WorkRootDir = (Get-Item -Path ".\" -Verbose).FullName
$RootDir = $WorkRootDir + "\.."
$BinRoot = $WorkRootDir + "\bin"
$GameVersion = ((git rev-list HEAD | measure-object).Count * 10) + 1100000
$LauncherVersion = $GameVersion
New-Item -ItemType Directory -Force $RootDir\caches | Out-Null
New-Item -ItemType Directory -Force $RootDir\caches\fivereborn | Out-Null
Push-Location $RootDir\caches
		
"<Caches>
		<Cache ID=`"fivereborn`" Version=`"$GameVersion`" />
</Caches>" | Out-File -Encoding ascii $RootDir\caches\caches.xml

Copy-Item -Force $BinRoot\five\release\FiveM.exe FiveM.exe
Copy-Item -Force $BinRoot\five\release\FiveM.exe CitizenFX.exe
 
if (Test-Path CitizenFX.exe.xz) {
		Remove-Item CitizenFX.exe.xz
}

Invoke-Expression "& $WorkRootDir\tools\ci\xz.exe -9 CitizenFX.exe"

$LauncherLength = (Get-ItemProperty CitizenFX.exe.xz).Length
"$LauncherVersion $LauncherLength" | Out-File -Encoding ascii version.txt

Copy-Item -Force $WorkRootDir\tools\ci\BuildCacheMeta.exe BuildCacheMeta.exe
Copy-Item -Force $WorkRootDir\tools\ci\DBNetwork.IndigoSxS.dll DBNetwork.IndigoSxS.dll
Copy-Item -Force $WorkRootDir\tools\ci\xz.exe xz.exe
 
Pop-Location

 # copy output files
Push-Location $RootDir\ext\ui-build
.\build.cmd

if ($?) {
		New-Item -ItemType Directory -Force $RootDir\caches\fivereborn\citizen\ui\ | Out-Null
		Copy-Item -Force -Recurse $RootDir\ext\ui-build\data\* $RootDir\caches\fivereborn\citizen\ui\
}

Pop-Location

Copy-Item -Force -Recurse $RootDir\vendor\cef\Release\*.dll $RootDir\caches\fivereborn\bin\
Copy-Item -Force -Recurse $RootDir\vendor\cef\Release\*.bin $RootDir\caches\fivereborn\bin\

New-Item -ItemType Directory -Force $RootDir\caches\fivereborn\bin\cef

Copy-Item -Force -Recurse $RootDir\vendor\cef\Resources\icudtl.dat $RootDir\caches\fivereborn\bin\
Copy-Item -Force -Recurse $RootDir\vendor\cef\Resources\*.pak $RootDir\caches\fivereborn\bin\cef\
Copy-Item -Force -Recurse $RootDir\vendor\cef\Resources\locales\en-US.pak $RootDir\caches\fivereborn\bin\cef\

Copy-Item -Force -Recurse $RootDir\data\shared\* $RootDir\caches\fivereborn\
Copy-Item -Force -Recurse $RootDir\data\client\* $RootDir\caches\fivereborn\

Copy-Item -Force $BinRoot\five\release\*.dll $RootDir\caches\fivereborn\
Copy-Item -Force $BinRoot\five\release\*.com $RootDir\caches\fivereborn\

Copy-Item -Force -Recurse $BinRoot\five\release\citizen\* $RootDir\caches\fivereborn\citizen\

